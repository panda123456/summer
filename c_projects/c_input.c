#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
//Complete the following function.

int main() {
    int n;
    scanf("%d\n", &n);

    int arr[n];

    int i = 0;
    int sum = 0;

    for (size_t i = 0; i < n; i++) {
      (scanf("%d", &arr[i]) == 1);
      sum += arr[i];
    }

    printf("%d\n", sum);
    return 0;
}
