#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>

int marks_summation(int* marks, int number_of_students, char gender) {
  int sum = 0;
  if(gender == 'b'){
    for (size_t i = 0; i < number_of_students; i = i + 2) {
      sum += marks[i];
    }
  }
  else if(gender == 'g'){
    for (size_t i = 1; i < number_of_students; i = i + 2) {
      sum += marks[i];
    }
  }
  else return -1;

  return sum;
}

int main() {
  int marks[] = {1,2,4,5,2};
  int x = marks_summation(marks, 5, 'g');
  printf("%d\n", x);

  return 0;
}
