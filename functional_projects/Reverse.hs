module Reverse where

rev :: [Int] -> [Int]
rev [] = []
rev xs | length xs <= 1 = xs
       | otherwise = last xs : ((rev ( (tail.init) xs)) ++ [head xs])
