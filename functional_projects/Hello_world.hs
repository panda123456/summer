module Hello_world where

hello_world :: IO ()
hello_world = putStrLn "Hello World"

recur :: Int -> IO ()
recur 1 = hello_world
recur x = do
    hello_world
    recur (x-1)

main :: IO ()
main = do
    n <- readLn :: IO Int
    recur n
